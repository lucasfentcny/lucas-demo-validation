/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.tncy.demo.validation;


import java.util.Calendar;
import java.util.Date;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author x-roy
 */
public class InstallationSiteValidationTest {

    private static Validator validator;

    public InstallationSiteValidationTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void validateMinimalSiteDef() {
        InstallationSite s = new InstallationSite("TELECOM Nancy");
        Set<ConstraintViolation<InstallationSite>> constraintViolations = validator.validate(s);
        assertEquals(0, constraintViolations.size());
    }

    @Test
    public void validateNullPerson() {
        InstallationSite s = new InstallationSite();
        Set<ConstraintViolation<InstallationSite>> constraintViolations = validator.validate(s);
        assertEquals(1, constraintViolations.size());
    }

    @Test
    public void validateAnonymous() {
        InstallationSite s = new InstallationSite("");
        Set<ConstraintViolation<InstallationSite>> constraintViolations = validator.validate(s);
        assertEquals(1, constraintViolations.size());
    }
    
    @Test
    public void validateFullSiteDef() {
        InstallationSite s = new InstallationSite("TELECOM Nancy", "193, avenue Paul Muller", "54602", "Villers-lès-Nancy", "FR", "0383682600", 48.6691922f, 6.1531142f);
        Set<ConstraintViolation<InstallationSite>> constraintViolations = validator.validate(s);
        assertEquals(0, constraintViolations.size());
    }
    
    
    @Test
    public void validateCrazyGPS() {
        InstallationSite s = new InstallationSite("TELECOM Nancy", "193, avenue Paul Muller", "54602", "Villers-lès-Nancy", "FR", "0383682600", 148.6691922f, 306.1531142f);
        Set<ConstraintViolation<InstallationSite>> constraintViolations = validator.validate(s);
        assertEquals(2, constraintViolations.size());
    }
    
    
    
//
//    @Test
//    public void validateAdult() {
//        Calendar calendar = Calendar.getInstance();
//        calendar.set(1974, Calendar.DECEMBER, 3);
//        Date birthDay = calendar.getTime();
//        Person p = new Person("Xavier", "Roy", birthDay, "FR");
//        Set<ConstraintViolation<Person>> constraintViolations = validator.validate(p, AdultCheck.class);
//        assertEquals(0, constraintViolations.size());
//    }
//    
//    @Test
//    public void validateChild() {
//        Calendar calendar = Calendar.getInstance();
//        calendar.set(2003, Calendar.SEPTEMBER, 10);
//        Date birthDay = calendar.getTime();
//        Person p = new Person("Sibylle", "Roy", birthDay, "FR");
//        Set<ConstraintViolation<Person>> constraintViolations = validator.validate(p, AdultCheck.class);
//        assertEquals(1, constraintViolations.size());
//    }
//    
//    @Test
//    public void validateAdultAnonymous() {
//        Person p = new Person("", "", null, "");
//        Set<ConstraintViolation<Person>> constraintViolations = validator.validate(p, AdultCheck.class); 
//        assertEquals(2, constraintViolations.size());
//    }
}
